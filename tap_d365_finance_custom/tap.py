"""D365FinanceCustom tap class."""

from typing import List

from singer_sdk import Stream, Tap
from singer_sdk import typing as th  # JSON schema typing helpers

from tap_d365_finance_custom.streams import (
    D365FinanceCustomStream,
    InventoryOnHandStream,
    PurchaseOrderLinesStream,
    ReleasedProductsStream,
    SalesInvoiceLinesStream,
    SalesOrderLinesStream,
    VendorsStream,
    SalesOrderHeadersV2Stream,
    SalesInvoiceHeadersV2Stream
)

STREAM_TYPES = [
    InventoryOnHandStream,
    PurchaseOrderLinesStream,
    ReleasedProductsStream,
    SalesInvoiceLinesStream,
    VendorsStream,
    SalesOrderHeadersV2Stream,
    SalesInvoiceHeadersV2Stream
]


class TapD365FinanceCustom(Tap):
    """D365FinanceCustom tap class."""

    name = "tap-d365-finance-custom"

    config_jsonschema = th.PropertiesList(
        th.Property(
            "api_key",
            th.StringType,
            required=True,
        ),
        th.Property(
            "subscription_key",
            th.StringType,
            required=True,
        ),
        th.Property(
            "api_url",
            th.StringType,
            default="https://api.mysample.com",
            required=True,
        ),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


if __name__ == "__main__":
    TapD365FinanceCustom.cli()
